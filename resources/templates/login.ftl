<html>
<head>
    <link rel="stylesheet" href="/static/styles.css">
</head>
<body>
<#if error??>
    <p style="color:red;">${error}</p>
</#if>
<#if message??>
    <p style="color:green;">${message}</p>
</#if>
<form action="/login" method="post" enctype="application/x-www-form-urlencoded">
    <div>Users:</div>
    <div><input type="text" name="username"/></div>
    <div>Password:</div>
    <div><input type="password" name="password"/></div>
    <div><input type="submit" value="Login"/></div>
</form>
<form action="/register" method="get" enctype="application/x-www-form-urlencoded">
    <div><input type="submit" value="Register"/></div>
</form>
</body>
</html>