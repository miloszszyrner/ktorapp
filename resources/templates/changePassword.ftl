<html>
<head>
    <link rel="stylesheet" href="/static/styles.css">
</head>
<body>
<#if error??>
    <p style="color:red;">${error}</p>
</#if>
<form action="/changePassword" method="post" enctype="application/x-www-form-urlencoded">
    <div>Old password:</div>
    <div><input type="password" name="oldPassword"/></div>
    <div>New Password:</div>
    <div><input type="password" name="newPassword"/></div>
    <div><input type="submit" value="Change password"/></div>
</form>
</body>
</html>