<#-- @ftlvariable id="data" type="com.dmcs.IndexData" -->
<html>
    <body>
        Hello ${user.login}!
        <form action="/changePassword" method="get" enctype="application/x-www-form-urlencoded">
            <div><input type="submit" value="Change password"/></div>
        </form>
        <form action="/logout" method="get" enctype="application/x-www-form-urlencoded">
            <div><input type="submit" value="Logout"/></div>
        </form>
    </body>
</html>
