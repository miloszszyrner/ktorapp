<html>
<head>
    <link rel="stylesheet" href="/static/styles.css">
</head>
<body>
<#if error??>
    <p style="color:red;">${error}</p>
</#if>
<form action="/register" method="post" enctype="application/x-www-form-urlencoded">
    <div>Users:</div>
    <div><input type="text" name="username"/></div>
    <div>Password:</div>
    <div><input type="password" name="password"/></div>
    <div>Confirm Password:</div>
    <div><input type="password" name="passwordConfirm"/></div>
    <div><input type="submit" value="Register"/></div>
</form>
</body>
</html>