package com.dmcs

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import kotlinx.html.*
import kotlinx.css.*
import freemarker.cache.*
import io.ktor.freemarker.*
import io.ktor.http.content.*
import io.ktor.sessions.*
import io.ktor.features.*
import io.ktor.request.receiveParameters
import org.jetbrains.exposed.sql.Database
import dao.DAOFacadeDatabase
import io.ktor.util.hex
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

val dao = DAOFacadeDatabase(Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver"))

val hashKey = hex("6819b57a326945c1968f45236589")

val hmacKey = SecretKeySpec(hashKey, "HmacSHA1")

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    dao.init()
    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
    }


    install(Sessions) {
        cookie<MySession>("SESSION")
    }

    routing {
        route("/login") {
            get {
                val user = call.sessions.get<MySession>()?.let { dao.getUser(it.id) }
                if (user != null) {
                    call.respondRedirect("/", permanent = false)
                } else {
                    call.respond(FreeMarkerContent("login.ftl", null))
                }
            }
                post {
                    val post = call.receiveParameters()
                    val userId = post["username"]
                    val password = post["password"]

                    val login = when {
                        userId == null -> null
                        password == null -> null
                        else -> dao.user(userId, hash(password))
                    }
                    if (login == null) {
                        call.respond(FreeMarkerContent("login.ftl", mapOf("error" to "Invalid login or password")))
                    } else {
                        call.sessions.set(MySession(login.id))
                        call.respondRedirect("/", permanent = false)
                    }
                }
        }

        route("/register") {
            get {
                val user = call.sessions.get<MySession>()?.let { dao.getUser(it.id) }
                if (user != null) {
                    call.respondRedirect("/", permanent = false)
                } else {
                    call.respond(FreeMarkerContent("register.ftl", null))
                }
            }
            post {
                val post = call.receiveParameters()
                when {
                    post["username"] == null -> call.respond(FreeMarkerContent("register.ftl", mapOf("error" to "Empty login")))
                    dao.getUserByLogin(post["username"]!!) != null -> call.respond(FreeMarkerContent("register.ftl", mapOf("error" to "Not unique login")))
                    post["password"] != post["passwordConfirm"] -> call.respond(FreeMarkerContent("register.ftl", mapOf("error" to "Passwords not the same")))
                    validatePassword(post["password"]) -> call.respond(FreeMarkerContent("register.ftl", mapOf("error" to "Password must have at least 5 characters and contains at least one capital letter and one special character.")))
                    else -> {
                        dao.createUser(post["username"]!!, hash(post["password"]!!))
                        call.respond(FreeMarkerContent("login.ftl", mapOf("message" to "Registration successful")))
                    }
                }

            }
        }

        route("/changePassword") {
            get {
                val user = call.sessions.get<MySession>()?.let { dao.getUser(it.id) }
                if (user != null) {
                    call.respond(FreeMarkerContent("changePassword.ftl", null))
                } else {
                    call.respond(FreeMarkerContent("login.ftl", null))
                }
            }
            post {
                val post = call.receiveParameters()
                val user = call.sessions.get<MySession>()?.let { dao.getUser(it.id) }
                when {
                    user!!.password != hash(post["oldPassword"]!!) -> call.respond(FreeMarkerContent("changePassword.ftl", mapOf("error" to "Incorrect current password")))
                    post["oldPassword"] == post["newPassword"] -> call.respond(FreeMarkerContent("changePassword.ftl", mapOf("error" to "Passwords are equal")))
                    validatePassword(post["newPassword"]) -> call.respond(FreeMarkerContent("changePassword.ftl", mapOf("error" to "Password must have at least 5 characters and contains at least one capital letter and one special character.")))
                    else -> {
                        dao.updatePassword(user.id, hash(post["newPassword"]!!))
                        call.respondRedirect("/", permanent = false)
                    }
                }

            }
        }

        get("/") {
            val user = call.sessions.get<MySession>()?.let { dao.getUser(it.id) }
            if (user != null) {
                call.respond(FreeMarkerContent("index.ftl", mapOf("user" to call.sessions.get<MySession>()?.let { dao.getUser(it.id) })))
            } else {
                call.respond(FreeMarkerContent("login.ftl", mapOf("error" to "User not logged in")))
            }
        }

        get("/logout") {
            call.sessions.clear<MySession>()
            call.respondRedirect("login")
        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color.red
                }
                p {
                    fontSize = 2.em
                }
                rule("p.myclass") {
                    color = Color.blue
                }
            }
        }

        // Static feature. Try to access `/static/ktor_logo.svg`
        static("/static") {
            resources("static")
        }

        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }

        }
    }
}
fun validatePassword(password: String?): Boolean {
    val regex = """^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#${'$'}%!\-_?&])(?=\S+${'$'}).{5,}""".toRegex()
    return  !regex.matches(password!!)
}

fun hash(password: String): String {
    val hmac = Mac.getInstance("HmacSHA1")
    hmac.init(hmacKey)
    return hex(hmac.doFinal(password.toByteArray(Charsets.UTF_8)))
}

data class MySession(val id: Int)

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}
