package dao

import model.User
import Users
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class DAOFacadeDatabase(val db: Database): DAOFacade{

    override fun init() = transaction(db) {
        SchemaUtils.create(Users)

    }
    override fun createUser(login: String, password: String) = transaction(db) {
        Users.insert {
            it[Users.login] = login; it[Users.password] = password;
        }
        Unit
    }
    override fun updateUser(id: Int, login:String, password:String) = transaction(db) {
        Users.update({Users.id eq id}){
            it[Users.login] = login
            it[Users.password] = password
        }
        Unit
    }
    override fun deleteUser(id: Int) = transaction(db) {
        Users.deleteWhere { Users.id eq id }
        Unit
    }
    override fun user(login: String, password: String) = transaction(db) {
        Users.select { Users.login eq login }
            .map {
                if (it[Users.password] == password) User(
                    it[Users.id], it[Users.login], it[Users.password]
                ) else
                    null
            }
            .singleOrNull()
    }

    override fun getUserByLogin(login: String) = transaction(db) {
        Users.select { Users.login eq login }.map {
            User(
                it[Users.id], it[Users.login], it[Users.password]
            )
        }.singleOrNull()
    }

    override fun getUser(id: Int) = transaction(db) {
        Users.select { Users.id eq id}.map {
            User(
                it[Users.id], it[Users.login], it[Users.password]
            )
        }.singleOrNull()
    }

    override fun getAllUsers() = transaction(db) {
        Users.selectAll().map {
            User(it[Users.id], it[Users.login], it[Users.password]
            )
        }
    }

    override fun updatePassword(id: Int, password: String) = transaction(db) {
        Users.update({Users.id eq id}){
            it[Users.password] = password
        }
        Unit
    }
    override fun close() { }
}