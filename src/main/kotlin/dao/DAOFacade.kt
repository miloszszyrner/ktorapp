package dao

import model.User
import java.io.Closeable

interface DAOFacade: Closeable {
    fun init()
    fun createUser(login:String, password:String)
    fun updateUser(id:Int, login:String, password:String)
    fun deleteUser(id:Int)
    fun user(login: String, password: String): User?
    fun getUser(id: Int): User?
    fun getUserByLogin(login:String): User?
    fun getAllUsers(): List<User>
    fun updatePassword(id: Int, password: String)
}