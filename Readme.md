##Dynamiczne języki skryptowe dla platformy Java EE

Application built using Kotlin and Ktor framework.  
Data is stored in in-memory h2 database.  
User can create account, login, logout and change its password.  
Additionally, there is validation for unique login and proper password.  
Furthermore password is hashed in db.
